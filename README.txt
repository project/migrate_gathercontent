
INTRODUCTION
------------

The Migrate GatherContent module allows you to import content from
GatherContent (https://gathercontent.com/) to your Drupal website.

This module provides a simple UI for creating migration entities and integrates
directly with the Migrate Tools interface.


FEATURES
------------

This module allows you to import content to almost any Content Entity including:
 * Nodes
 * Taxonomy Terms
 * Files
 * Comments
 * Paragraphs
 * Media
 * And More...

You can also establish entity relationships by creating Migration Dependencies.
This is how you can create nodes with entity references like paragraphs, media,
taxonomy terms etc.

By virtue of using Migrate you can extend this module through custom plugins and
alter hooks.


INSTALLATION
------------

0 - Prerequisites:
Requires Drupal Core Migrate, Migrate Plus, Migrate Tools modules as well as
Chepper's GatherContent API (this should automatically be installed
through composer). You will also need a GatherContent Account and
API Key.
More information: https://docs.gathercontent.com/reference

2 - Configuration:
After successful installation provide your email and API key. Choose which
projects you want to use. This will expose the list of available GatherContent
Templates that you can map too.


HOW TO USE
------------

The UI is built with Drupal core's Migrate process in mind. Like in Drupal Migrate
the general process for importing content is as follows.

 1. Create a Migration group where you want to store your migrations.
    Note, it's recommended to create one group per content type.

 2. In that group create a migration for each entity type that you want to create.
    E.g Nodes, paragraphs, taxonomy terms etc.

 3. Enable migration dependencies on the host entity when you want to import and
    "attach" paragraphs, media etc.

 4. Import content, this will go through the normal migration import pipeline. You
    can use Drush or the UI.

KNOWN ISSUES
------------

1 - Migration Dependency order is not honored during UI import.

This module allows you to set the order of migration dependencies. However there is an issue with the Migrate Tools
module that prevents dependencies from executing in a predictable order while using the UI. This does not affect the
drush import.


