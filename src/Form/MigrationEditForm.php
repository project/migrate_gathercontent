<?php

namespace Drupal\migrate_gathercontent\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_gathercontent\Plugin\GatherContentFieldPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate_gathercontent\DrupalGatherContentClient;
use Drupal\field\Entity\FieldConfig;
use Drupal\Component\Utility\NestedArray;

/**
 * Form handler for the Example add and edit forms.
 */
class MigrationEditForm extends EntityForm {


  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manger.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityFieldManager;

  /**
   * Drupal GatherContent Client.
   *
   * @var \drupal\migrate_gathercontent\drupalgathercontentclient
   */
  protected $client;

  /**
   * Migration Plugin Manager
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * The gathercontent field plugin manager.
   *
   * @var \Drupal\migrate_gathercontent\Plugin\GatherContentFieldPluginManager $gatherContentFieldPluginManager
   */
  protected $gatherContentFieldPluginManager;

  /**
   * Array of writable fields.
   */
  protected $writableFields;

  /**
   * Constructs an MigrationEditForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *    The entityTypeManager service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *    The entityFieldManager service.
   * @param \Drupal\migrate_gathercontent\DrupalGatherContentClient $gathercontent_client
   *    The Drupal GatherContent client service.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *    The Migraton Plugin Manager service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityFieldManager $entity_field_manager, DrupalGatherContentClient $gathercontent_client, MigrationPluginManagerInterface $migration_plugin_manager, GatherContentFieldPluginManager $gathercontent_field_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->client = $gathercontent_client;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->gatherContentFieldPluginManager = $gathercontent_field_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('migrate_gathercontent.client'),
      $container->get('plugin.manager.migration'),
      $container->get('plugin.manager.migrate.gathercontent_field')
    );
  }

  /**
   * Helper function for fetching the migration value of a process.
   *
   * @param $process_pipeline
   * @param null $field
   *
   * @return array|bool
   */
  private function getProcessMigration($process_pipeline, $field = NULL) {

    // TODO: Consolidate this with getProcessKeyValue.
    // A separate function was necessary because of custom logic that
    // has to happen for the source field.
    if (!empty($process_pipeline[$field])) {
      $process_pipeline = $process_pipeline[$field];
    }

    foreach ($process_pipeline as $key => $process) {
      if ($key === 'migration') {
        $result = [];
        $result = array_merge($result, [$process]);
        return $result;
      }
      elseif (is_array($process)) {
        $result = $this->getProcessMigration($process);
        if (!empty($result)) {
          return $result;
        }
      }
    }
    return FALSE;
  }

  /**
   * Helper function for fetching the first occurrence of a specific key in a
   * process.  Used for default values.
   *
   * @param $process_pipeline
   * @param $process_key
   * @param null $field
   * @return array|bool
   */
  private function getProcessKeyValue($process_pipeline, $process_key, $field = NULL) {

    if (!empty($process_pipeline[$field])) {
      $process_pipeline = $process_pipeline[$field];
    }

    foreach ($process_pipeline as $key => $process) {
      if ($key === 'source') {
        // Normalize the source.
        $result = [];
        if (is_string($process)) {
          $process = [$process];
        }

        foreach ($process as $source) {
          // If source starts with @ then find its pseudo field.
          if (strpos($source, '@') === 0) {
            $source = substr($source, 1);
            $process_pipeline = $this->entity->get('process');
            $data = $this->getProcessKeyValue($process_pipeline, $process_key, $source);
          }
          else {
            $data = [$source];
          }
          $result = array_merge($result, $data);
        }
        return $result;
      }
      // If key matches the process key then return the result.
      elseif ($key === $process_key) {
        $result = [];
        $result = array_merge($result, [$process]);
        return $result;
      }
      elseif (is_array($process)) {
        $result = $this->getProcessKeyValue($process, $process_key);
        if (!empty($result)) {
          return $result;
        }
      }
    }
    return FALSE;
  }


  /**
   * Build a row with field select list.
   *
   * @param array $source
   * @param string $field_name
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildFieldRow(array $source, $field_name, array $form, FormStateInterface $form_state) {

    $field_row = [];

    $field_row['label'] = [
      '#plain_text' => $source['label'],
    ];

    $field_row['source'] = [
      '#type' => 'hidden',
      '#default_value' => $source['id'],
    ];

    $field_row['entity_field'] = [
      '#type' => 'container',
    ];

    // Getting list of field options for this entity.
    $fields = $this->getWritableFields($this->entity);
    $field_options = [];
    foreach($fields as $id => $field) {
      if ($field instanceof FieldConfig) {
        $field_options['Fields'][$id] = $field->getLabel();
      }
      else {
        $field_options['Properties'][$id] = $field->getlabel();
      }
    }

    $field_row['entity_field']['field_select'] = [
      '#type' => 'select',
      '#options' => $field_options,
      '#empty_option' => $this->t('- Do not map -'),
      '#option_value' => NULL,
      '#default_value' => $field_name,
    ];

    return $field_row;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildMigrationRow($migration, $field_name, array $form, FormStateInterface $form_state) {

    $migration_dependencies = $this->entity->get('migration_dependencies');

    // Note: We don't actually store the weight with the migration entity.
    $weight = 0;

    $field_row = [
      // Making this table draggable.
      '#attributes' => [
        'class' => ['draggable'],
      ],
      '#weight' => $weight,
      'import' => [
        '#type' => 'checkbox',
        '#title' => $migration->label(),
        '#default_value' => (!empty($migration_dependencies['required']) && in_array($migration->id(), $migration_dependencies['required'])),
      ],
      'entity_field' => [
        '#type' => 'markup',
        '#markup' => 'N/A',
      ],
      'weight' => [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => ['class' => ['migrations-order-weight']]
      ],
    ];

    // we can only map migrations to directly to fields that were part of
    // the same source/template. this is because we use item id as the
    // source for migration_lookup.
    // this makes it possible to create relationships for paragraphs, media
    // etc.
    // todo: remove this restriction after you have added field settings anc
    // can change the source.
    $field_options = [];
    $fields = $this->getWritableFields($this->entity, [
      'entity_reference',
      'entity_reference_revisions'
    ]);
    if (!empty($fields)) {
      foreach ($fields as $id => $field) {
        $field_options['Fields'][$id] = $field->getLabel();
      }
    }

    $dependency_source = $migration->get('source');
    $source = $this->entity->get('source');
    if ($dependency_source['template'] == $source['template']) {
      $field_row['entity_field'] = [
        '#type' => 'container',
      ];
      $field_row['entity_field']['field_select'] = [
        '#type' => 'select',
        '#options' => $field_options,
        '#empty_option' => $this->t('- Do not map -'),
        '#option_value' => null,
        '#default_value' => $field_name,
        '#states' => [
          'disabled' => [
            ':input[name="migration_mappings[' . $migration->id() .'][import]"]' => ['checked' => false],
          ]
        ]
      ];
    }

    return $field_row;

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    // Getting list of field options for this entity.
    $fields = $this->getWritableFields($entity);
    $field_options = [];
    foreach($fields as $id => $field) {
      if ($field instanceof FieldConfig) {
        $field_options['Fields'][$id] = $field->getLabel();
      }
      else {
        $field_options['Properties'][$id] = $field->getlabel();
      }
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Label for this mapping."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    // Getting list of groups
    $groups = \Drupal::entityTypeManager()->getStorage('migration_group')->loadMultiple();
    $group_options = [];
    foreach($groups as $id => $group) {
      $group_options[$id] = $group->label();
    }

    $form['migration_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Group'),
      '#required' => TRUE,
      '#description' => $this->t('Choose the Group for this mapping.'),
      '#default_value' => ($entity->get('migration_group')) ? $entity->get('migration_group') : 'default',
      '#options' => $group_options,
    ];

    // Parsing the process values into something we can use on the form.
    $process = $this->entity->get('process');

    // Getting default values for fields and migrations.
    $default_values = [];

    // Getting any migrations.
    foreach ($process as $dest => $processes) {
      $explode = explode('/', $dest);
      $dest = $explode[0];
      $migrations = $this->getProcessMigration($process,  $dest);
      if (!empty($migrations)) {
        foreach ($migrations as $migration) {
          $default_values[$migration] = $dest;
        }
      }
    }

    // Fields
    foreach ($process as $dest => $processes) {
      $sources = $this->getProcessKeyValue($process, 'source', $dest);
      // TODO: Need to add support for sub-fields.
      $explode = explode('/', $dest);
      $dest = $explode[0];

      if (!empty($sources)) {
        foreach ($sources as $source) {
          $default_values[$source] = $dest;
        }
      }
    }

    $form['field_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Fields'),
      '#open' => TRUE,
    ];

    // Header settings.
    $form['field_details']['fields'] = [
      '#type' => 'table',
      '#attributes' => ['id' => 'edit-fields'],
      '#header' => [
        ['data' => ['#markup' => $this->t('Source')], 'width' => '50%', 'colspan' => 2],
        ['data' => ['#markup' => $this->t('Field')], 'colspan' => 4],
      ],
    ];

    $source_plugin = $this->migrationPluginManager->createInstance($entity->id())->getSourcePlugin();
    $source_fields = $source_plugin->fields();

    foreach($source_fields as $id => $label) {
      $field = (!empty($default_values[$id])) ? $default_values[$id] : FALSE;
      $source = [
        'id' => $id,
        'label' => $label,
      ];
      $form['field_details']['fields']['#tree'] = TRUE;
      $form['field_details']['fields'][$id] = $this->buildFieldRow($source, $field, $form, $form_state);
    }


    // Loading all migration entities.
    // Only load migrations that are part of this group.
    $migration_entities = $this->entityTypeManager->getStorage('migration')->getQuery()
      ->condition('migration_group', $entity->get('migration_group'))
      ->condition('id', $entity->id(), '<>')
      ->execute();


    if (!empty($migration_entities)) {
      $form['mapping_migrations'] = [
        '#title' => $this->t('Migration Dependencies'),
        '#type' => 'details',
        '#open' => TRUE,
        '#description' => $this->t("Add other migrations as dependencies. You can also map migrations with the same template to fields on this entity. This is useful for entity reference fields like media, paragraphs etc.")
      ];
      $form['mapping_migrations']['migration_mappings'] = [
        '#type' => 'table',
        '#header' => [
          ['data' => ['#markup' => $this->t('Migration')], 'width' => '50%'],
          ['data' => ['#markup' => $this->t('Field')], 'width' => '50%'],
          'weight' => '',
        ],
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'migrations-order-weight',
          ]
        ],
      ];

      // Sorting migrations by dependency order.
      $migration_dependencies = $this->entity->get('migration_dependencies');
      if (!empty($migration_dependencies)) {
        $migration_entities = array_unique(array_merge($migration_dependencies['required'], array_values($migration_entities)));
      }

      foreach ($migration_entities as $id) {
        $migration = $this->entityTypeManager->getStorage('migration')->load($id);

        $field_name = (isset($default_values[$id])) ? $default_values[$id] : false;
        $form['mapping_migrations']['migration_mappings'][$id] = $this->buildMigrationRow($migration, $field_name, $form, $form_state);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Helper function for fetching writeable fields.
   * @param $entity
   * @param array $field_types
   * @return array
   */
  private function getWritableFields($entity, $field_types = []) {

    $id = $entity->id();

    if (empty($this->writableFields[$id])) {
      // Getting list of field options for this entity.
      // Fetching
      $destination = $entity->get('destination');
      // Get entity type.
      $entity_type = explode(':', $destination['plugin']);
      $entity_type = array_pop($entity_type);

      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $destination['default_bundle']);
      $writeable_fields = [];
      foreach ($fields as $fid => $field) {
        // Only map writable fields.
        // TODO: Some writable fields we probably don't want to write to.
        // e.g 'default revision'
        if (!$field->isReadOnly()) {
           /* // Altering the field name (where applicable).
          try {
            // TODO: This is causing fatal error.
            // Probably shouldn't change field name unless we actually using that field.
            $plugin_id = $this->gatherContentFieldPluginManager->getPluginIdFromFieldType($type, []);
            if (!empty($plugin_id)) {
              $plugin = $this->gatherContentFieldPluginManager->createInstance($plugin_id, []);
              $plugin->alterFieldName($fid);
            }
          } catch (PluginNotFoundException $exception) {

          }*/
          $writeable_fields[$fid] = $field;
        }
      }
      $this->writableFields[$id] = $writeable_fields;
    }

    // Filter out field types where applicable.
    // TODO: Come back to this.
    if (!empty($field_types)) {
      $filtered_fields = [];
      foreach ($this->writableFields[$id] as $fid => $field) {
        $type = $field->getType();
        if (in_array($type, $field_types)) {
          $filtered_fields[$fid] = $field;
        }
      }
      return $filtered_fields;
    }

    return $this->writableFields[$id];

  }

  /**
   * {@inheritdoc}
   *
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Migration dependencies.
    $dependencies = [];

    // Normalizing the field and migrations. Processing the field mappings
    // Merge field and migration mappings.
    $field_mappings = $form_state->getValue('field_mappings');
    $migration_mappings = $form_state->getValue('migration_mappings');
    if (!empty($migration_mappings)) {
      $field_mappings = NestedArray::mergeDeep($field_mappings, $migration_mappings);
    }

    foreach ($field_mappings as $source => $field_info) {
      // Field mappings
      $field = $field_info['entity_field'];
      if (!empty($field)) {
        $mappings_normalized[$field]['source'][] = $source;
      }

      // Add required migration dependencies. Note this will be
      // reversed because the order of the migrations matters.
      if (!empty($field_info['import']) && $this->exist($source)) {
        $dependencies['required'][] = $source;
      }
    }

    // TODO: Save this in variable.
    $destination = $this->entity->get('destination');
    $plugin = explode(':', $destination['plugin']);
    $entity_type = array_pop($plugin);
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $destination['default_bundle']);

    // Creating stub migration.
    $definition = [];
    $migration = $this->migrationPluginManager->createStubMigration($definition);

    foreach ($mappings_normalized as $field => $source) {
      $process_plugin = $migration->getProcessPlugins();

      // Checking for compound sources.
      // If there are any use that instead.
      if (count($source['source']) > 1) {
        $this->createCompoundSource($field, $source['source'], $migration);
        $source = '@_compound_' . $field;
      }
      else {
        $source = array_pop($source['source']);
      }

      // Only add process plugin
      if (!empty($field_definitions[$field]) && empty($process_plugin[$field])) {
        $type = $field_definitions[$field]->getType();
        $plugin_id = $this->gatherContentFieldPluginManager->getPluginIdFromFieldType($type, [], $migration);
        if (!empty($plugin_id)) {
          $plugin = $this->gatherContentFieldPluginManager->createInstance($plugin_id, [], $migration);
          $plugin->defineValueProcessPipeline($migration, $field, $source, $this->entity);
        }
        // If no field type process exists them use default process.
        else {
          // List fields do not work with deltas on multi value fields.
          $process = [
            'plugin' => 'get',
            'source' => $source,
          ];
          $migration->mergeProcessOfProperty($field, $process);
        }
      }
    }
    $process = $migration->getProcess();
    $this->entity->set('process', $process);

    // Set migration dependencies.
    $this->entity->set('migration_dependencies', array_reverse($dependencies));

    if ($this->entity->save()) {

      // Invalidating the cache so that it gets rebuilt after saving the entity.
      $this->migrationPluginManager->clearCachedDefinitions();

      $params = [
        'migration_group' => $form_state->getValue('migration_group'),
      ];
      $form_state->setRedirect('entity.migration.list', $params);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($this->entity->save()) {

      // Invalidating the cache so that it gets rebuilt after saving the entity.
      $this->migrationPluginManager->clearCachedDefinitions();

      $params = [
        'migration_group' => $form_state->getValue('migration_group'),
      ];
      $form_state->setRedirect('entity.migration.list', $params);
    }
  }


  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    // TODO: This is an expensive function that gets called a lot.
    // Migration dependencies.
    $dependencies = [];

    // Normalizing the field and migrations. Processing the field mappings
    // Merge field and migration mappings.
    $field_mappings = $form_state->getValue('fields');
    $migration_mappings = $form_state->getValue('migration_mappings');
    if (!empty($migration_mappings)) {
      $field_mappings = NestedArray::mergeDeep($field_mappings, $migration_mappings);
    }

    foreach ($field_mappings as $source => $field_info) {
      // Field mappings
      if (!empty($field_info['entity_field']['field_select'])) {
        $field = $field_info['entity_field']['field_select'];
        $mappings_normalized[$field]['source'][] = $source;
      }

      // Add required migration dependencies. Note this will be
      // reversed because the order of the migrations matters.
      if (!empty($field_info['import']) && $this->exist($source)) {
        $dependencies['required'][] = $source;
      }
    }

    // TODO: Save this in variable.
    $destination = $entity->get('destination');
    $plugin = explode(':', $destination['plugin']);
    $entity_type = array_pop($plugin);
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $destination['default_bundle']);

    // Creating stub migration.
    $definition = [];
    $migration = $this->migrationPluginManager->createStubMigration($definition);

    // Setting existing process plugins.
    $process = $entity->get('process');
    $migration->setProcess($process);

    foreach ($mappings_normalized as $field => $field_info) {
      // Checking for compound sources.
      // If there are any use that instead.
      if (count($field_info['source']) > 1) {
        $this->createCompoundSource($field, $field_info['source'], $migration);
        $source = '@_compound_' . $field;
      }
      else {
        $source = array_pop($field_info['source']);
      }

      // If there is no process then generate it.
      if (!empty($field_definitions[$field])) {
        $type = $field_definitions[$field]->getType();
        $plugin_id = $this->gatherContentFieldPluginManager->getPluginIdFromFieldType($type, [], $migration);
        if (!empty($plugin_id)) {
          $plugin = $this->gatherContentFieldPluginManager->createInstance($plugin_id, [], $migration);
          $plugin->defineValueProcessPipeline($migration, $field, $source, $entity);
        }
        // If no field type process exists them use default process.
        else {
          // List fields do not work with deltas on multi value fields.
          $process = [
            'plugin' => 'get',
            'source' => $source,
          ];
          $migration->mergeProcessOfProperty($field, $process);
        }
      }
    }
    $process = $migration->getProcess();
    $entity->set('process', $process);

    // Set migration dependencies.
    $entity->set('migration_dependencies', array_reverse($dependencies));

  }

  /**
   * This combines values from multiple sources into a single source.
   *
   * @param $field_name
   * @param $entity
   * @return array
   */
  private function createCompoundSource($field_name, $multi_sources, $migration) {
    // TODO: refactor this to make it more concise.
    // Creating prepared sources.
    if (!empty($multi_sources)) {
      $prepared_sources = [];
      foreach ($multi_sources as $source) {
        $process = [];
        // If the source is a migration then look up those values first.
        $lookup_migration = $this->entityTypeManager->getStorage('migration')->load($source);
        if ($lookup_migration) {
          $process[] = [
            'plugin' => 'migration_lookup',
            'migration' => $lookup_migration->id(),
            'source' => 'id',
          ];
        }
        // Otherwise use normal 'get' plugin.
        else {
          $process[] = [
            'plugin' => 'get',
            'source' => $source,
          ];
        }

        $migration->setProcessOfProperty('_prepare_' . $source, $process);
        $prepared_sources[] = '@_prepare_' . $source;
      }

      if (!empty($prepared_sources)) {
        // Creating the compound field, we refer to this field as the source.
        // e.g $source = '@_compound_' . $field_name.
        $process = [];
        $process[] = [
          'plugin' => 'get',
          'source' => $prepared_sources,
        ];

        // Only arrays from fields should be flattened.
        if (!$this->exist($source)) {
          $process[] = [
            'plugin' => 'flatten',
          ];
        }
        $migration->setProcessOfProperty('_compound_' . $field_name, $process);
      }
    }

    return $process;
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('migration')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}