<?php

namespace Drupal\migrate_gathercontent\Controller;

use Drupal\Core\Url;
use Drupal\migrate_tools\Controller\MigrationListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of migration entities in a given group.
 *
 * @package Drupal\migrate_tools\Controller
 *
 * @ingroup migrate_tools
 */
class GatherContentMigrationListBuilder extends MigrationListBuilder {

  /**
   * Builds a row for a migration plugin.
   *
   * @param \Drupal\Core\Entity\EntityInterface $migration_entity
   *   The migration plugin for which to build the row.
   *
   * @return array|null
   *   A render array of the table row for displaying the plugin information.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildRow(EntityInterface $migration) {

    $row = parent::buildRow($migration);

    // Add edit and delete operations if it's gathercontent.
    $source = $migration->get('source');
    if ($source['plugin'] == 'gathercontent') {
      if ($migration->get('source')) {
        $row['operations']['data']['#links'] += [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('entity.migration.edit_form', [
              'migration' => $migration->id(),
            ]),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.migration.delete_form', [
              'migration' => $migration->id(),
            ]),
          ],
        ];
      }
    }
    return $row;
  }

}
