<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Plugin\PluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\MigrateFieldInterface;

/**
 * The base class for all migrate gathercontent field plugins.
 *
 * @see \Drupal\migrate\Plugin\MigratePluginManager
 * @see \Drupal\migrate_gathercontent\Annotation\MigrateGatherContentField
 * @see \Drupal\migrate_gathercontent\Plugin\GatherContentFieldInterface
 * @see plugin_api
 *
 * @ingroup migration
 */
// TODO: Add interface.
abstract class FieldPluginBase extends PluginBase {

  /**
   * Apply any custom processing to the field bundle migrations.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration entity.
   * @param string $field_name
   *   The field name we're processing the value for.
   * @param string $source
   *   The source value.
   * @param entity $entity
   *   The array of field data from FieldValues::fieldData().
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {
    $process = [
      'plugin' => 'get',
      'source' => $source,
    ];
    $migration->mergeProcessOfProperty($field_name, $process);
  }

  /**
   * {@inheritdoc}
   */
  public function alterFieldMigration(MigrationInterface $migration) {
    /*$process[0]['map'][$this->pluginId][$this->pluginId] = $this->pluginId;
    $migration->mergeProcessOfProperty('type', $process);
    */
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldType(Row $row) {
    $field_type = $row->getSourceProperty('type');

    if (isset($this->pluginDefinition['type_map'][$field_type])) {
      return $this->pluginDefinition['type_map'][$field_type];
    }
    else {
      return $field_type;
    }
  }

}
