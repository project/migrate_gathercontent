<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "date",
 *   label = @Translation("Date"),
 *   field_types = {
 *     "datetime",
 *   }
 * )
 */
class DateField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    // TODO: Need to make this more flexible.
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strip_tags',
      'source' => $source,
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'trim',
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strtotime',
    ];
    $process[] = [
      'plugin' => 'format_date',
      'from_format' => 'U',
      'to_format' => DateTimeItemInterface::DATETIME_STORAGE_FORMAT
    ];

    $migration->setProcessOfProperty($field_name, $process);

  }

}
