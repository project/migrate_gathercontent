<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "timestamp",
 *   label = @Translation("Date"),
 *   field_types = {
 *     "timestamp",
 *   }
 * )
 */
class TimestampField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    // TODO: Need to make this more flexible.
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strip_tags',
      'source' => $source,
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'trim',
    ];
    $process[] = [
      'plugin' => 'callback',
      'callable' => 'strtotime',
    ];

    $migration->setProcessOfProperty($field_name, $process);

  }

}
