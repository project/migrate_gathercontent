<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "list",
 *   label = @Translation("List"),
 *   field_types = {
 *     "list_string",
 *     "list_float",
 *     "list_integer",
 *   }
 * )
 */
class ListField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {
    // List fields do not work with deltas on multi value fields.
    $process = [
      'plugin' => 'get',
      'source' => $source,
    ];
    $migration->setProcessOfProperty($field_name, $process);

  }

}
