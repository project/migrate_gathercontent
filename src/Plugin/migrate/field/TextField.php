<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "text",
 *   label = @Translation("Text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class TextField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    $process = [
      'plugin' => 'get',
      'source' => $source,
    ];
    $migration->setProcessOfProperty($field_name . '/value', $process);

    $formats = filter_formats();
    $names = array_keys($formats);
    $format = $names[0];
    $process = [
      'plugin' => 'default_value',
      'default_value' => $format,
    ];
    $migration->setProcessOfProperty($field_name . '/format', $process);

  }

}
