<?php

namespace Drupal\migrate_gathercontent\Plugin\migrate\field;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Plugin implementation of the 'string' field.
 *
 * @GatherContentField(
 *   id = "file",
 *   label = @Translation("File"),
 *   field_types = {
 *     "file",
 *   }
 * )
 */
class FileField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $source, $entity) {

    $process = [
      'plugin' => 'sub_process',
      'source' => $source,
      'process' => [
        'target_id' => [
          'plugin' => 'gathercontent_file_import',
          'id_only' => TRUE,
          'destination_field' => $field_name,
          'source' => [
            'url',
            'filename',
          ],
        ],
        'title' => 'filename',
      ]
    ];

    $migration->setProcessOfProperty($field_name, $process);

  }

}
